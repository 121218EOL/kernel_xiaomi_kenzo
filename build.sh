#!/bin/bash
#
# Pure Kernel build script
#
# Copyright (C) 2017 Ashish Malik (im.ashish994@gmail.com)
# Copyright (C) 2018 Dmitry Porshnev (dmitryporshnevdev@gmail.com)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#functions
function echo() {
    command echo -e "$@"
}

function header_success() {
    echo ${GREEN}
    echo "====$(for i in $(seq ${#1}); do echo "=\c"; done)===="
    echo "==  ${1}  =="
    echo "====$(for i in $(seq ${#1}); do echo "=\c"; done)===="
    echo ${RST}
}

function header_error() {
    echo ${RED}
    echo "====$(for i in $(seq ${#1}); do echo "=\c"; done)===="
    echo "==  ${1}  =="
    echo "====$(for i in $(seq ${#1}); do echo "=\c"; done)===="
    echo ${RST}
}

#colors
RED="\033[0;31m"
GREEN="\033[0;32m"
RST="\033[0m"
BROWN="\033[0;33m"
CYAN="\033[0;36m"
NC="\033[0m"
BOLD="\033[1m"

#directories
KERNEL_DIR=$PWD
KERN_IMG=$KERNEL_DIR/arch/arm64/boot/Image.gz-dtb
CONFIG_DIR=$KERNEL_DIR/arch/arm64/configs

#export
export CROSS_COMPILE="/home/angga/build/aarch64-linux-gnu-6.x-kernel-pure-linaro-master/bin/aarch64-linux-gnu-"
export ARCH=arm64
export SUBARCH=arm64
export KBUILD_BUILD_USER="root"
export KBUILD_BUILD_HOST="SentinelPRIMEs"

#misc
CONFIG=lineageos_kenzo_defconfig
THREAD="-j$(grep -c ^processor /proc/cpuinfo)"

#main script
while true; do
echo -e "\n${NC}[1]Build kernel"
echo -e "[2]Regenerate defconfig"
echo -e "[3]Source cleanup"
echo -e "[4]Quit${NC}"
echo -ne "\n${CYAN}Please enter a choice[1-4]:${NC} "

read choice

if [ "$choice" == "1" ]; then
  echo " "
  BUILD_START=$(date +"%s")
  DATE=`date`
  echo "${NC}Build started at $DATE${NC}"
  make lineageos_kenzo_defconfig
  make menuconfig
  make $THREAD

  if ! [ -a $KERN_IMG ]; then
    header_error "KERNEL COMPILATION FAILED"
    exit 1
  fi
  BUILD_END=$(date +"%s")
  DIFF=$(($BUILD_END - $BUILD_START))
  header_success "BUILD SUCCESSFUL"
  echo "${BOLD}Script duration: $(($DIFF / 60)) MINUTES AND $(($DIFF % 60)) SECONDS.${NC}"
fi


if [ "$choice" == "2" ]; then
  echo " "
  make $CONFIG
  cp .config $CONFIG_DIR/$CONFIG
  header_success "DEFCONFIG GENERATED"
fi


if [ "$choice" == "3" ]; then
  make clean &>/dev/null
  make mrproper &>/dev/null
  header_success "KERNEL SOURCES CLEANED"
fi


if [ "$choice" == "4" ]; then
 exit 1
fi
done
